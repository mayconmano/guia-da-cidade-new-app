angular.module('config', [])
.constant('DB_CONFIG', {
    name: 'qwguiaMobile',
    tables: [
      {
            name: 'menus',
            columns: [
                {name: 'id', type: 'integer'},
                {name: 'parent_id', type: 'integer NULL'},
                {name: 'lft', type: 'integer NULL'},
                {name: 'rght', type: 'integer NULL'},
                {name: 'nome', type: 'text'},
                {name: 'fl_pagina', type: 'boolean'},
            ],
            primaryKey: ['id']
        },        
        {
            name: 'paginas',
            columns: [
                {name: 'id', type: 'integer'},                
                {name: 'menu_id', type: 'integer NULL'}, 
                {name: 'usuario_id', type: 'integer NULL'}, 
                {name: 'titulo', type: 'text'},
                {name: 'resumo', type: 'text'},
                {name: 'texto', type: 'text'},
                {name: 'logomarcar', type: 'text'},
                {name: 'telefone', type: 'text'},
                {name: 'telefone2', type: 'text'},
                {name: 'celular', type: 'text'},
                {name: 'responsavel', type: 'text'},
                {name: 'tel_responsavel', type: 'text'},
                {name: 'coordenadas', type: 'text'},
                {name: 'categoria_id', type: 'integer NULL'},
                {name: 'subcategoria_id', type: 'integer NULL'},
                {name: 'palavra_chave', type: 'text'},
                {name: 'data_inicio', type: 'date'},
                {name: 'data_fim', type: 'date'},
                {name: 'data_expiracao', type: 'date'},
                {name: 'preco', type: 'text'},
                {name: 'local', type: 'text'},
                
            ],
            primaryKey: ['id']
        },
        {
            name: 'config',
            columns: [
                {name: 'id', type: 'integer'},                
                {name: 'nome', type: 'text'},
                {name: 'valor', type: 'text'}                
            ],
            primaryKey: ['id']
        },
    ]
});