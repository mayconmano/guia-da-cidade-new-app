var app = angular.module("app", ['onsen', 'service', 'lang', 'angular-loading-bar']);
var heightDevice = null;

/**
 * Perform functions when starting app
 * @param {object} $rootScope 
 * @param {object} $window
 * @param {object} DB 
 * @param {object} $translate
 */
app.run(function($rootScope, $window, DB, $translate, Menu) {
   
        
    DB.init();    
    
    Menu.getMenuPrincipal();

});

app.controller('homeCtrl', function($scope, Menu, Pagina, Webservice) {       
    
    Menu.getMenuPrincipal().then(function(result){          
        $scope.menus = result;
    });
    
    $scope.destination = function(menu){   
        console.log(menu);
        if(menu.fl_pagina == "false"){
            Menu.getMenuSubmenu(menu.id).then(function(result){
                if(result.length == 0) result = false;
                                
                $scope.menus = result;                
                navi.insertPage(-1, 'home.html', {'animation': 'none'});   
                console.log(navi.getPages());
            });                        
        }else{
            Pagina.getDetailPage(menu.pag_id).then(function(result){                                
                navi.pushPage('interna.html', { 'animation': 'none', 'data': result });
            });
            
        }
    }
    
    $scope.verifyNewVersion = function(){
        ons.notification.confirm({
            message: 'Deseja verificar nova versão?',
            title: 'Atualização',
            callback: function(idx) {
                switch (idx) {
                    case 0:
                        break;

                    case 1:
                        Webservice.getVersion().then(function(result){
                            if(result){
                                Menu.getMenuPrincipal().then(function(result){          
                                    $scope.menus = result;
                                    ons.notification.alert({
                                        message: 'Versão atualizado com sucesso',
                                        title: 'Atualização',
                                    });
                                });                                
                            }else{
                                ons.notification.alert({
                                    message: 'Sua versão é a ultima!',
                                    title: 'Atualização',
                                });
                            }
                        });
                        break;
                }
            }
        });
    }
    
});

app.controller('internaCtrl', function($scope) {        
   
    var page = navi.getCurrentPage();
    $scope.details = page.options.data;
   
});