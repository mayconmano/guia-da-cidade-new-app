var Service = angular.module("service", ['config', 'lang']);

/**
* Database
* @param {object} $q 
* @param {object} DB_CONFIG
*/
Service.factory('DB', function($q, DB_CONFIG) {
    var self = this;
    self.db = null;
 
    /**
    * Initialize the database by creating all the tables and their settings.
    */
    self.init = function() {
        
        /** 
        * PRODUCTION
        */
        //self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name}); 
        
        /**
        * DEVELOPMENT
        */
        self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);

        angular.forEach(DB_CONFIG.tables, function(table) {
            var columns = [];
            var primaryKey = [];
            
            angular.forEach(table.columns, function(column) {
                columns.push(column.name + ' ' + column.type);
            });
            
            angular.forEach(table.primaryKey, function(column) {
                primaryKey.push(column);
            });
                        
                        
            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ', PRIMARY KEY(' + primaryKey.join(',') +') )';                        
            self.query(query);
            
            var query = 'INSERT INTO config (id, nome, valor) VALUES (1, "versao", "0")';                        
            self.query(query);
             
            console.log('Table ' + table.name + ' initialized');
            
            
        });  
        
    };
 
    /**
    * function that executes all commands on the database
    * @param {String} query
    * @param {Array} bindings
    * return object
    */
    self.query = function(query, bindings) {
        
        bindings = typeof bindings !== 'undefined' ? bindings : [];
        var deferred = $q.defer();
 
        self.db.transaction(function(transaction) {
            transaction.executeSql(query, bindings, function(transaction, result) {                 
                deferred.resolve(result);                
            }, function(transaction, error) {
                deferred.reject(error);
            });
        });
    
        return deferred.promise;
    };
    
    /**
    * Fetch all
    * @param {Object} result
    * return object
    */
    self.fetchAll = function(result) {
        var output = [];
 
        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }
        
        return output;
    };
    
    /**
    * Fetch first
    * @param {Object} result
    * return object
    */
    self.fetch = function(result) {

        if (result != null && result.rows != null && result.rows.length > 0) {
            return result.rows.item(0);
        }else{
            return false;
        }
    };
 
    return self;
});
              

/**
* Menu
* @param {object} DB 
*/
Service.factory('Menu', function(DB) {
    var self = this;
    var table = 'menus';
    
    
    /**
    * Catch URL webservice
    * return object
    */
    self.getMenuPrincipal = function(){
        return DB.query('SELECT m.id, m.nome, p.id pag_id, p.menu_id, m.fl_pagina FROM '+table+' m '+
                        ' LEFT JOIN paginas p ON p.menu_id = m.id WHERE coalesce(m.parent_id, "") = "" ', [])
        .then(function(result){               
            return DB.fetchAll(result);
        });
    }
    
    self.getMenuSubmenu = function(id){
        return DB.query('SELECT m.id, m.nome, p.id pag_id, p.menu_id, m.fl_pagina FROM '+table+' m '+
                        ' LEFT JOIN paginas p ON p.menu_id = m.id WHERE m.parent_id = "'+id+'" ', [])
        .then(function(result){               
            return DB.fetchAll(result);
        });
    }       
  
    
    return self;
});

/**
* Pagina
* @param {object} DB 
*/
Service.factory('Pagina', function(DB) {
    var self = this;
    var table = 'paginas';
    
    self.getDetailPage = function(id){
        return DB.query('SELECT * FROM '+table+' WHERE id = ? ', [id])
        .then(function(result){               
            return DB.fetch(result);
        });
    }

  
    
    return self;
});


/**
* Config
* @param {object} DB 
*/
Service.factory('Config', function(DB) {
    var self = this;
    var table = 'config';
    
    self.getVersao = function(){
        return DB.query('SELECT valor FROM '+table+' WHERE nome = ? ', ['versao'])
        .then(function(result){               
            return DB.fetch(result);
        });
    }

    self.update = function(valor, nome){
        return DB.query('UPDATE '+table+' SET valor = ? WHERE nome = ? ', [valor, nome])
        .then(function(result){               
            return result;
        });
    }
    
    return self;
});


/**
* Webservice
* @param {object} DB 
*/
Service.factory('Webservice', function(DB, $http, Config) {
    var self = this;    
    
    self.getVersion = function(){
        var req = {
            method: 'GET',
            url: 'http://54.94.226.70/webservice/versao',            
        }                                        
        return $http(req).then(function (response) { 
            var versao_servidor = response.data.Versao.id;            
            return Config.getVersao().then(function(result){
                var versao_interna = result.valor;                                
                if(parseInt(versao_servidor) > parseInt(versao_interna)){
                    return self.updateBaseLocal().then(function(retorno){
                        return Config.update(versao_servidor, 'versao').then(function(){                            
                            return retorno;
                        });
                        
                    });
                }else{
                    return false;
                }
            });
        });
    }
    
    self.updateBaseLocal = function(){
    
        var req = {
            method: 'GET',
            url: 'http://54.94.226.70/webservice/atualizacao',            
        }                                        
        return $http(req).then(function (response) {                        
            angular.forEach(response.data, function(data, tabela){
                DB.query('DELETE FROM '+tabela+' ');
                DB.db.transaction(function(transaction) {  
                    angular.forEach(data, function(colum){                        
                        var colunas = [];
                        var resultados = [];
                        var valores = [];
                        angular.forEach(colum, function(value, key){ 
                            colunas.push(key);
                            valores.push(value);
                            resultados.push('?');
                        });                                             
                        transaction.executeSql('INSERT OR IGNORE INTO '+tabela+' ('+colunas.join(',')+') VALUES ('+resultados.join(',')+')', 
                                               valores,
                                               null,
                                              erroExecuteSql);                        
                    });
                });                  
            });
            return true;
        });
        
    }

  
    
    return self;
});
  
function erroExecuteSql(transacation, erro){
    console.log('Oops.  Error was '+erro.message+' (Code '+erro.code+')');
}

function dataHandler(transaction, results)
{
    // Handle the results
    var string = "Green shirt list contains the following people:\n\n";
    for (var i=0; i<results.rows.length; i++) {
        // Each row is a standard JavaScript array indexed by
        // column names.
        var row = results.rows.item(i);
        string = string + row['name'] + " (ID "+row['id']+")\n";
    }
    console.log(string);
}